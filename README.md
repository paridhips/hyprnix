# Credit

This worked is derived from [Tyler Kelley 's Amazing Work](https://gitlab.com/Zaney/zaneyos). 
[His YouTube Channel](https://www.youtube.com/@ZaneyOG)
[His Discord Server](https://discord.gg/2cRdBs8)


# Installation
## 1. Create The Directory

    $ cd ~/ && mkdir .nixos
    $ cd .nixos
    $ mkdir <uniqueDeviceName>

### Note:
Tyler has separate configuration for his workstation and laptop. I have added qemu files as well. This guide assumes that you are creating a new device for yourself.  If you want to use one the the existing configurations,Replace the   ` hardware.nix`  file in one of the existing device folder that you wish to use

## 2. Clone The Repo

  

    $ cd .nixos
    $ git clone https://gitlab.com/paridhips/hyprnix.git  

   

  
     
## 3. Source the following files and change their ownership

 
 - `/etc/nixos/hardware.nix`

 
`$ sudo cp /etc/nixos/hardware.nix ~/.nixos/<uniqueDeviceName>/hardware.nix` 

`$ sudo chown <yourUserName>:users ~/.nixos/< uniqueDeviceName>/hardware.nix`
    
 ### Note
  **Copy the configuration files from one of the other devices on the repo like from the workstation.** 
## 4. Add the new device to your flake file.

Add The following for your device in `flake.nix`

```
 qemu= nixpkgs.lib.nixosSystem {
 specialArgs = { inherit system; inherit inputs; 
 inherit username; inherit hostname; inherit gitUsername;
 inherit gitEmail; inherit theLocale; inherit theTimezone;
 inherit wallpaperDir; inherit wallpaperGit;
 };
 modules = [ ./qemu/configuration.nix
 home-manager.nixosModules.home-manager {
 home-manager.extraSpecialArgs = { inherit username; 
 inherit gitUsername; inherit gitEmail; inherit inputs; inherit theme;
 inherit browser; inherit wallpaperDir; inherit wallpaperGit; inherit flakeDir;
 inherit (inputs.nix-colors.lib-contrib {inherit pkgs;}) gtkThemeFromScheme;
 };
 home-manager.useGlobalPkgs = true;
 home-manager.useUserPackages = true;
 home-manager.users.${username} = import ./home.nix;
 }
 ];
 };

 ```

   ## 5. Change the variables in flake file
| Variables |
|--|--|
| `hostname` |
| `username` |
| `gitUsername` |
| `theLocale` |
| `theme` |
| `browser` |
| `wallpaperGit`|

## 6. Rebuild Nix

    `$ sudo nixos-rebuild switch --flake .#<uniqueDeviceName>`


